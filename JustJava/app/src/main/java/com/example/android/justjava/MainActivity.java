package com.example.android.justjava;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.NumberFormat;

public class MainActivity extends AppCompatActivity {

    int quantity = 1;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void increment(View view) {
        if (quantity == 100)
            Toast.makeText(this, getString(R.string.increment_warning), Toast.LENGTH_SHORT).show();

        else
            displayQuantity(++quantity);
    }

    public void decrement(View view) {
        if (quantity == 1)
            Toast.makeText(this, getString(R.string.decrement_warning), Toast.LENGTH_SHORT).show();

        else
            displayQuantity(--quantity);
    }

    public void submitOrder(View view) {
        CheckBox whippedCreamCheckBox = (CheckBox) findViewById(R.id.whipped_cream_checkbox);
        CheckBox chocolateCheckBox = (CheckBox) findViewById(R.id.chocolate_checkbox);
        EditText nameEditText = (EditText) findViewById(R.id.name_edit_text);
        EditText emailEditText = (EditText) findViewById(R.id.email_edit_text);
        createOrderSummary(calculatePrice(whippedCreamCheckBox.isChecked(), chocolateCheckBox.isChecked()), whippedCreamCheckBox.isChecked(), chocolateCheckBox.isChecked(), nameEditText.getText().toString(), emailEditText.getText().toString());
    }

    public int calculatePrice(boolean hasWhippedCream, boolean hasChocolate) {
        int price = 5;
        if (hasWhippedCream)
            price += 1;
        if (hasChocolate)
            price += 2;
        price *= quantity;
        return price;
    }

    private void displayQuantity(int number) {
        TextView quantityTextView = (TextView) findViewById(R.id.quantity_text_view);
        quantityTextView.setText("" + number);
    }

    private void createOrderSummary(int price, boolean hasWhippedCream, boolean hasChocolate, String name, String email) {
        String summary = getString(R.string.name_summary, name);
        summary += "\n" + getString(R.string.whipped_cream_summary, hasWhippedCream);
        summary += "\n" + getString(R.string.chocolate_summary, hasChocolate);
        summary += "\n" + getString(R.string.quantity_summary, quantity);
        summary += "\n" + getString(R.string.total_summary, NumberFormat.getCurrencyInstance().format(price));
        summary += "\n" + getString(R.string.thank_you);

        Intent mailIntent = new Intent(Intent.ACTION_SENDTO);
        mailIntent.setType("message/rfc822");
        mailIntent.setData(Uri.parse(getString(R.string.mail_to) + email));
        mailIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.mail_subject) + name);
        mailIntent.putExtra(Intent.EXTRA_TEXT, summary);
        try {
            startActivity(mailIntent);
        }
        catch (ActivityNotFoundException ex) {
            Toast.makeText(this, getString(R.string.activity_exception), Toast.LENGTH_SHORT).show();
        }
    }
}